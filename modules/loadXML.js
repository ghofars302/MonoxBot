const xml2json = require('xml2json');
const fs = require('fs');

module.exports = (path, _options) => {
	let results;
	
	try {
		const xml = fs.readSync(path).toString();
		results = xml2json.toJson(xml);
	} catch (err) {
		throw err;
	}
	
	return results;
};
		