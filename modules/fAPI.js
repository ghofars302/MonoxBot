const fetch = require('node-fetch');
const package = require('../package.json');
const bufferType = require('buffer-type');

const fAPI = async function fAPI(endpoint, options) {
	const data = {
		method: "POST",
		headers: {
			"User-Agent": `MonoxBot Framework v${package.version}`,
			"Authorization": process.env.matmen,
			"Content-Type": Buffer.isBuffer(options.buffer) ? bufferType(options.buffer).type : "application/json"
		},
		body: Buffer.isBuffer(options.buffer) ? options.buffer : JSON.stringify({
			images: options.images,
			args: options.args
		})
	}

	const body = await fetch(`https://fapi.wrmsr.io/${endpoint}`, data);

	if (body.status !== 200) {
		const text = await body.text();

		return Promise.reject(new fAPI.Error(text));
	}

	if (body.headers.get('content-type') === 'application/json') {
		return body.json();
	} else {
		return body.buffer();
	}
}

fAPI.path = fetch('https://fapi.wrmsr.io/pathlist').then(a => a.json()).then(a => a.paths);

fAPI.Error = class fAPIError extends Error {
	constructor(reason) {
		super(reason);

		this.name = 'fAPIError';
	}
}

module.exports = fAPI;