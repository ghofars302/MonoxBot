const PrettyDate = require('../../modules/PrettyDate');

module.exports = {
	description: 'Replies with the user\'s stats',
	category: 'Utils',
	args: '<@Mentions | User>',
	aliases: ['userinfo', 'whois', 'user', 'u'],
	cooldown: 5000,
	run: async function (ctx, { argsString } ) {
		let user = ctx.author;
		
		if (argsString) {
		    try {
		        const match = await ctx.bot.utils.getUser(ctx, argsString);
		        user = match;
		    } catch (error) {
		        return error;
		    }
		}

		const serverShardResults = await this.client.shard.broadcastEval(`this.guilds.filter(g => g.members.has('${user.id}')).map(g => g.name)`);
		let servers = [];
		for (const serverShardResult of serverShardResults) servers = servers.concat(serverShardResult);
		const shownServers = servers.slice(-3);

		let body = `\`${shownServers.join('`, `')}`;
		body += shownServers.length < servers.length ? `\` + ${servers.length - shownServers.length} more` : '`';
		
		const status = user.presence.status;
		const activity = user.presence.activity;
		const guild = ctx.guild ? ctx.guild.member(user) : false;

		const embed = new ctx.bot.api.MessageEmbed();
		
		embed.setTitle(`Userinfo ${user.tag}`);
		embed.setThumbnail(user.displayAvatarURL());
		embed.addField(`UserID`, user.id);
		
		if (ctx.guild && ctx.guild.member(user).username) embed.addField(`Username`, ctx.guild.member(user).username);
		
		embed.addField(`User status:`, status);
		if (activity) {
		    switch (activity.type) {
		        case null:
		            break;
		        case 'LISTENING': {
		            const music = activity.details;
		            const album = activity.assets.largeText;
		            const author = activity.state;
		        
		            embed.addField(`Listening Spotify`, `Music: ${music}\nAlbum: ${album} by ${author}`);
		            break;
		        }
		        case 'STREAMING': {
		            const game = activity.details;
		            const url = activity.url;
		            const title = activity.name;
		            
		            embed.addField(`Streaming in Twitch.tv`, `Stream Title: ${title}\nTwitch URL: ${url}\nPlaying Game: ${game}`);
		            break;
		        }
		        default: {
		            switch (activity.name) {
    		            case 'Visual Studio': {
		                    const workingOn = activity.state ? activity.state : null;
		                    const editing = activity.details ? activity.details : null;
		                
		                    embed.addField(`Editing in Visual Studio`, `${workingOn|| 'No data'}\n${editing|| 'No data'}`);
		                    break;
		                }
		                case 'Visual Studio Code': {
		                    const workingOn = activity.state ? activity.state : null;
		                    const editing = activity.details ? activity.details : null;
		                
		                    embed.addField(`Editing in Visual Studio Code`, `${workingOn|| 'No data'}\n${editing|| 'No data'}`);
		                    break;
		                }
		                case 'Fortnite': {
		                    const statusGame = activity.details;
		                    const teamMode = activity.state;
		                
		                    embed.addField(`Playing Fornite`, `Status: ${statusGame || 'No data'}\nTeam mode: ${teamMode || 'No data'}`);
		                    break;
		                }
		                case 'foobar2000': {
		                    const music = activity.details;
		                    const author = activity.state;
		                
		                    embed.addField(`Listening foobar2000`, `Music: ${music}\bAuthor: ${author}`);
		                    break;
		               }
		               default: {
		                    embed.addField(`Playing`, `${activity.name}`);
		               }
		            }
		        }
		    }
		}
		embed.addField(`User Join Date`, PrettyDate(user.createdTimestamp));
		
		if (ctx.guild) embed.addField(`Member Guild Join Date`, PrettyDate(guild.joinedTimestamp));
		
		embed.addField(`Seen on ${servers.length} servers`, body);
		embed.setTimestamp();
		embed.setFooter(`monoxbot.ga`, ctx.main.user.displayAvatarURL());
		
		return embed;
	}
};

