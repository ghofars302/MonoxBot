module.exports = {
    description: 'Identify image using Microsoft CaptionBot',
    args: '<@Mentions | User | URL>',
    cooldown: 5000,
    run: async function (ctx, { args }) {
        const images = await ctx.bot.utils.getImagesFromMessage(ctx.message, args);
        
        if (images.length === 0) {
            if (['^'].includes(args[0]) || args.length === 0) {
                return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from recent messages');
            }

            return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from the query');
        }

        const res = await ctx.bot.fetch('https://captionbot.azurewebsites.net/api/messages', {
        	method: 'POST',
            headers: {
            	'Content-Type': 'application/json; charset=utf-8',
                'User-Agent': 'MonoxBot project Fetch'
            },
            body: JSON.stringify({
            	"Content": images[0],
                "Type": "CaptionRequest"
            })
        });
        
        if (res.status !== 200) return ':x: `Error while call the remote API`';
        
        const text = await res.json();
        
        return text;
    }
}