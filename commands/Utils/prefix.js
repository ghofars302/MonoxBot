module.exports = {
    description: 'Change guild prefix',
    guildOnly: true,
    args: '<Prefix> [reset|clear]',
    cooldown: 10000,
    run: async function (ctx, { argsString }) {
        if (argsString) {
			if (!ctx.member.hasPermission('MANAGE_GUILD') && !ctx.bot.utils.isAdmin(ctx.author.id)) return ':x: Only guild administrators can change the prefix'

			if (['reset', 'clear', ctx.main.prefix].includes(argsString.toLowerCase())) {
				await ctx.bot.utils.queryDB('UPDATE settings SET value = $1 WHERE server = $2', [null, ctx.guild.id]);
                await ctx.guild.initPrefix()
				return `Prefix reset to \`${ctx.main.prefix}\``
			} else {
				const check = await ctx.bot.utils.queryDB('SELECT * FROM settings WHERE server = $1', [ctx.guild.id]);
                if (check.rowCount < 1) {
                	await ctx.bot.utils.queryDB('INSERT INTO settings(server, setting, value) VALUES ($1, $2, $3)', [ctx.guild.id, 'prefix', argsString]);
                } else {
                	await ctx.bot.utils.queryDB('UPDATE settings SET value = $1 WHERE server = $2', [argsString, ctx.guild.id]);
                }
                await ctx.guild.initPrefix()
				return `Prefix set to \`${argsString}\``
			}
		} else {
			const prefixResult = await ctx.bot.utils.queryDB('SELECT value FROM settings WHERE setting = $1 AND server = $2', ['prefix', ctx.guild.id]);
			const prefix = prefixResult.rowCount > 0 ? prefixResult.rows[0].value : ctx.main.prefix;

			return `Current prefix: \`${prefix || ctx.main.prefix}\``
		}
    }
}