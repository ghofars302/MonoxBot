const { MessageEmbed } = require('discord.js');

module.exports = {
    description: 'Search query on duckduckgo.com',
    args: '<Query>',
    aliases: ['ddg', 'ducksearch'],
    cooldown: 5000,
    run: async function (ctx, { argsString }) {
        if (!argsString) return `\`\`\`${ctx.prefix}duckduckgo <Query>\n\nSearch query on duckduckgo.com\`\`\``;

        const text = await ctx.bot.fAPI('duckduckgo', {
            args: {
                text: argsString
            }
        });

        if (!text.cards && text.results.length < 1) return 'Not found.';
        
        const list = createArrayItems(text);
        
        const message = await ctx.reply(list[0]);
        
        if (list.length > 1) {
        	const paginator = ctx.bot.Paginate.initPaginate(message, ctx.author, list.length, true);
        
        	paginator.on('paginate', async num => {
        		console.log(num);
        		await message.edit(list[num - 1]);
        	});
        	return true;
        } else {
        	return true;
        }
    }
}

const createArrayItems = (text) => {
	const array = [];
	const tempArray = [];
	
	let pageCount = 0;
	let pageArrayLength = [];
	
	let iteration = 0;
	let count = 1;
	
	if (text.cards) {
		const embedCard = new MessageEmbed()
			.setTitle(text.cards.title)
			.setDescription(text.cards.description);
			
		array.push(embedCard);
		pageCount += 1;
		pageArrayLength.push('a');
	}
	
	let output = '';
	
	text.results.forEach(item => {
		output += `${item.title}\n`;
		output += `${item.link}\n`;
		
		tempArray[count - 1] = output;
		
		iteration += 1;
		
		if (iteration > 10) {
			count += 1;
			iteration = 0;
			output = '';
		}
	});
	
	tempArray.forEach(x => {
		const embed = new MessageEmbed()
			.setTitle('Search results')
			.setDescription(`${x}`)
			
		array.push(embed);
		pageCount += 1;
	});
	
	let i = 1;
	
	for (const foot of array) {
		foot.setFooter(`Page ${i} of ${pageCount}`, 'https://maxcdn.icons8.com/Share/icon/Logos/duckduckgo1600.png');
		i += 1;
	}
	
	return array;
}