const cowsay = require('cowsay');

module.exports = {
	description: 'Create ascii cow saying your thing',
	args: '<text>',
	cooldown: 3000,
	run: (ctx, { argsString }) => {
		if (!argsString) return ctx.bot.messageHandler.invalidArguments(ctx, 'Missing text');
		
		const cow = cowsay.say({
			text: ctx.bot.api.Util.escapeMarkdown(argsString),
		});
		
		return `\`\`\`${cow}\`\`\``;
	}
}