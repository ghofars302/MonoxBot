const {stripIndent} = require('common-tags');

module.exports = {
    description: 'Get invite of this bot',
    category: 'Utils',
    cooldown: 60000,
    run: async function (ctx) {
        if (ctx.main.secret && !ctx.bot.utils.isAdmin(ctx.author.id)) return 'Sorry, but invite for this bot currently disable for development testing';

        const embed = new ctx.bot.api.MessageEmbed()
            .setDescription(stripIndent`
            	**MonoxBot Invite**
            
                [With Administrator](https://discordapp.com/api/oauth2/authorize?client_id=${ctx.main.user.id}&permissions=8&scope=bot)
                [Without Administrator](https://discordapp.com/api/oauth2/authorize?client_id=${ctx.main.user.id}&permissions=522304&scope=bot) (RECOMMENDED)
                
            	If you have question or reporting bug(s) join
                [Support Server](https://discord.gg/AamgnqS)
            `)
            .setFooter('monoxbot.ga', ctx.main.user.displayAvatarURL())
            .setTimestamp()

        return embed;
    }
}