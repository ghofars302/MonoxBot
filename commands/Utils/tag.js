const {
	stripIndent
} = require('common-tags');

const bufferType = require('buffer-type');
const Parser = require('../../const/Parser')

module.exports = {
	description: 'Base command for tags',
	category: 'Utils',
	args: {
		text: '<Tag> | create | owner | dump | list | delete | raw | gift | random',
		type: 'tag'
	},
	aliases: ['t'],
	cooldown: 1000,
	run: async function (ctx, {
		key,
		splitArgs,
		stringArgs
	}) {
		
		console.log(key, splitArgs, stringArgs);
		
		if (!key) return invalidArgument(ctx)

		if (['add', 'create'].includes(key.toLowerCase())) {

			if (splitArgs.length < 2) return ':x: `Please input tag content to create`';
			const name = splitArgs[0].toLowerCase()

			if (['add', 'create', 'edit', 'gift', 'dump', 'delete', 'owner', 'view', 'raw', 'rename'].includes(name)) return ':x: `You can\'t create tag with that name because that was keyword`';

			const content = splitArgs.splice(1, splitArgs.length).join(' ');
			if (content.length < 1) return ':x: `You must input content`';

			const tags = await this.utils.queryDB('SELECT content FROM tags WHERE name = $1', [name]);
			if (tags.rowCount > 0) return `:x: Tag **${name}** already exists!`
			await ctx.bot.utils.queryDB('INSERT INTO tags VALUES ($1, $2, $3)', [name, content, ctx.author.id]);
			return `:white_check_mark: Created tag **${name}**!`

		} else if (key.toLowerCase() === 'edit') {

			if (splitArgs.length < 2) return ':x: `You must input new tag content to edit it`';

			const name = splitArgs[0].toLowerCase();
			const content = splitArgs.splice(1, splitArgs.length).join(' ');

			if (content.length < 1) return ':x: `You must input content`';

			const tag = await this.utils.queryDB('SELECT userid FROM tags WHERE name = $1', [name]);
			if (tag.rowCount < 1) return `:x: \`Tag **${name}** not found!\``
			if (!this.utils.isAdmin(ctx.author.id) && ctx.author.id !== tag.rows[0].userid) return ':x: `You don\'t own that tag!`'

			await this.utils.queryDB('UPDATE tags SET content = $2 WHERE name = $1', [name, content]);
			return `:pencil: Updated tag **${name}**`

		} else if (key.toLowerCase() === 'rename') {

			if (splitArgs.length < 3) return ':x: `You must input new tag name`';

			const name = splitArgs[0].toLowerCase();

			const tag = await this.utils.queryDB('SELECT userid FROM tags WHERE name = $1', [name]);
			if (tag.rowCount < 1) return `:x: Tag **${name}** not found!`
			if (!this.utils.isAdmin(ctx.author.id) && ctx.author.id !== tag.rows[0].userid) return ':x: `You don\'t own that tag!`';

			const newName = splitArgs[1].toLowerCase();

			await this.utils.queryDB('UPDATE tags SET name = $2 WHERE name = $1', [name, newName]);
			return `:pencil: Renamed tag **${name}** to **${newName}**`

		} else if (key.toLowerCase() === 'gift') {
			if (ctx.isDM) return ':x: `You can\'t use gift tag when in outside Guild`';

			if (splitArgs.length < 2) return ':x: `You must input tag name to gift`';

			const name = splitArgs[0].toLowerCase();
			let user = ctx.author;

			if (splitArgs[1]) {
				const match = this.utils.getMemberFromString(ctx.message, splitArgs[1]);
				if (match) user = match.user;
			}

			const tag = await this.utils.queryDB('SELECT userid FROM tags WHERE name = $1', [name]);
			if (tag.rowCount < 1) return `:x: Tag **${name}** not found!`
			if (!this.utils.isAdmin(ctx.author.id) && ctx.author.id !== tag.rows[0].userid) return ':x: `You don\'t own that tag!`'

			if (user.id === ctx.author.id) return ':x: `You cant gift tags to yourself!`';
			if (user.id === this.client.user.id) return ':x: `You cant gift tags to me!`';
			if (user.bot) return ':x: `You can\'t gift tag to Bot`';

			const msg = await ctx.reply(`<@!${user.id}>, **${ctx.author.tag}** want give you tag **${name}**\nSay **yes** to accept it, Say **no** to reject it`);
			const AwaitMsg = await msg.channel.awaitMessages(m => m.author.id === user.id && ['yes', 'y', 'no', 'n'].includes(m.content.toLowerCase()), {
				max: 1,
				time: 10000
			});

			if (AwaitMsg.array().length === 0) {
				return msg.edit(':x: `Action canceled because timeout`');
			}

			const confirmed = AwaitMsg.first();
			if (['no', 'n'].includes(confirmed.content.toLowerCase())) {
				return msg.edit(`:x: \`Action canceled, because ${user.tag} reject it\``);
			}

			await this.utils.queryDB('UPDATE tags SET userid = $2 WHERE name = $1', [name, user.id]);
			return msg.edit(`:gift: Gifted tag **${name}** to **${user.tag}**`);

		} else if (['delete', 'remove'].includes(key.toLowerCase())) {

			if (splitArgs.length < 1) return ':x: `You must input tag`';

			const name = splitArgs.splice(0, splitArgs.length).join(' ').toLowerCase(); // eslint-disable-line newline-per-chained-call

			const tag = await this.utils.queryDB('SELECT userid FROM tags WHERE name = $1', [name]);
			if (tag.rowCount < 1) return `:x: Tag **${name}** not found!`
			if (!this.utils.isAdmin(ctx.author.id) && ctx.author.id !== tag.rows[0].userid) return ':x: `You don\'t own that tag!`'

			await this.utils.queryDB('DELETE FROM tags WHERE name = $1', [name]);
			return `:wastebasket: Deleted tag **${name}**`

		} else if (['raw', 'view'].includes(key.toLowerCase())) {

			if (splitArgs.length < 1) return ':x: `You must input tag`';

			const name = splitArgs[0].toLowerCase();

			const tag = await this.utils.queryDB('SELECT content FROM tags WHERE name = $1', [name]);
			if (tag.rowCount < 1) return `:x: Tag **${name}** not found!`

			return ctx.reply(tag.rows[0].content, {
				code: true
			});

		} else if (key.toLowerCase() === 'owner') {

			if (splitArgs.length < 1) return ':x: `Please input tag name`';

			const name = splitArgs.splice(0, splitArgs.length).join(' ').toLowerCase(); // eslint-disable-line newline-per-chained-call

			const tag = await this.utils.queryDB('SELECT userid FROM tags WHERE name = $1', [name]);
			if (tag.rowCount < 1) return `:x: Tag **${name}** not found!`

			const userID = tag.rows[0].userid;
			const user = ctx.users.has(userID) ? ctx.users.get(userID) : await ctx.users.fetch(userID);

			return `:bust_in_silhouette: Tag **${name}** is owned by **${user.tag}**`

		} else if (key.toLowerCase() === 'list') {

			if (splitArgs[0] === 'all') {

				const tags = await this.utils.queryDB('SELECT name FROM tags');

				return ctx.reply(`All tags (\`${tags.rowCount}\`):`, {
					files: [{
						attachment: Buffer.from(tags.rows.map(r => r.name).join('\n'), 'utf-8'),
						name: 'alltags.txt'
					}]
				});

			} else {

				let user = ctx.author;

				if (splitArgs[0]) {
					const match = this.utils.getMemberFromString(ctx.message, splitArgs[0]);
					if (match) user = match.user;
				}

				const tags = await this.utils.queryDB('SELECT name FROM tags WHERE userid = $1', [user.id]);

				const tagList = tags.rows.map(r => r.name.includes(' ') ? `"${r.name}"` : r.name).join('\n') || 'This user made no tags'; // eslint-disable-line no-confusing-arrow

				if (tagList.length > 2048) {

					return ctx.reply(`\`${user.tag}\`'s tags (\`${tags.rowCount}\`):`, {
						files: [{
							attachment: Buffer.from(tagList, 'utf-8'),
							name: `tags-${user.username}-${user.discriminator}.txt`
						}]
					});

				} else {
					const embed = new ctx.bot.api.MessageEmbed();

					embed.setTitle(`${user.tag}'s Tags`);
					embed.setDescription(tagList);
					embed.setColor(0xff3366);

					return ctx.reply({
						embed
					});
				}

			}

		} else if (key.toLowerCase() === 'dump') {

			let user = ctx.author;

			if (args[1]) {
				const match = ctx.bot.utils.getMemberFromString(ctx.message, args[1]);
				if (match) user = match.user;
			}

			const tags = await ctx.bot.utils.queryDB('SELECT name, content FROM tags WHERE userid = $1', [user.id]);

			return ctx.reply(`Tag dump for \`${user.tag}\` (\`${tags.rowCount}\` Tags):`, {
				files: [{
					attachment: Buffer.from(JSON.stringify(tags.rows, null, 4), 'utf-8'),
					name: `tagdump-${user.username}-${user.discriminator}.json`
				}]
			});

		} else if (key.toLowerCase() === 'random') {

			const allTags = await ctx.bot.utils.queryDB('SELECT * FROM tags');

			const randomResult = allTags.rows.random();

			return `Tag **${randomResult.name}**\n${ctx.bot.utils.filterMentions(randomResult.content)}`
		} else if (key.toLowerCase() === 'test') {
			const ptest = new Parser(this.client, ctx);
			
		    let parsed = await ptest.parse(stringArgs, [], { content: stringArgs, name: 'test', ownerid: ctx.main.user.id, test: true });
			
			let text = parsed.result;
			let attachments = parsed.attachments;
			let imagescripts = parsed.imagescripts;
			
			if (parsed.nsfw) {
				attachments = [];
				imagescripts = [];
				text = ':underage: `NSFW Results, please switch to channel tagged as NSFW in its settings`';
			}
			
			const options = {
				files: []
			}
			
			let isError = { error: false };
			let k = 0;
			
			for (let i of attachments) {
				if (isError.error) continue;
				
				try {
					let buffer = await ctx.bot.fetch(i.url).then(a => a.buffer());
					options.files.push({ name: i.name ? i.name : (bufferType(buffer).type === 'image/gif' ? `image${k}.gif` : `image${k}.png`), attachment: buffer});
					+k;
				} catch (error) {
					isError.error = error;
				}
			}
			
			for (let i of imagescripts) {
				if (isError.error) continue;
				
				try {
					let buffer = await ctx.bot.fAPI('parse_tag', {
						args: {
							text: i.script
						}
					});
					
					options.files.push({ name: i.name ? i.name : (bufferType(buffer).type === 'image/gif' ? `image${k}.gif` : `image${k}.png`), attachment: buffer});
					+k;
				} catch (error) {
					isError.error = error;
				}
			}
			
			if (isError.error) {
				text = `:warning: ${isError.error.message}`;
				options.files = [];
			}
			
			if (text.length < 1 && options.files.length === 0) text = ':warning: `Result return empty output`';
			
			text = ctx.bot.utils.filterMentions(text);
			
			return ctx.reply(text, options)
		} else {
			const peval = new Parser(this.client, ctx);

			const res = await ctx.bot.utils.queryDB('SELECT * FROM tags WHERE name = $1', [key]);
			if (res.rowCount < 1) return `:x: Tag **${key}** not found!`
			
			const tag = res.rows[0];
			
			let parsed = await peval.parse(tag.content, stringArgs.split(/ +/g), tag);
			
			let text = parsed.result;
			let attachments = parsed.attachments;
			let imagescripts = parsed.imagescripts;
			
			if (parsed.nsfw) {
				attachments = [];
				imagescripts = [];
				text = ':underage: `NSFW Results, please switch to channel tagged as NSFW in its settings`';
			}
			
			const options = {
				files: []
			}
			
			let isError = { error: false };
			let k = 0;
			
			for (let i of attachments) {
				if (isError.error) continue;
				
				try {
					let buffer = await ctx.bot.fetch(i.url).then(a => a.buffer());
					options.files.push({ name: i.name ? i.name : (bufferType(buffer).type === 'image/gif' ? `image${k}.gif` : `image${k}.png`), attachment: buffer});
					++k;
				} catch (error) {
					isError.error = error;
				}
			}
			
			for (let i of imagescripts) {
				if (isError.error) continue;
				
				try {
					let buffer = await ctx.bot.fAPI('parse_tag', {
						args: {
							text: i.script
						}
					});
					
					options.files.push({ name: i.name ? i.name : (bufferType(buffer).type === 'image/gif' ? `image${k}.gif` : `image${k}.png`), attachment: buffer});
					++k;
				} catch (error) {
					isError.error = error;
				}
			}
			
			if (isError.error) {
				text = `:warning: ${isError.error.message}`;
				options.files = [];
			}
			
			if (text.length < 1 && options.files.length === 0) text = ':warning: `Result return empty output`';
			
			text = ctx.bot.utils.filterMentions(text);
			
			return ctx.reply(text, options)
		}
	}
};

const invalidArgument = (ctx) => stripIndent `
  \`\`\`
  ${ctx.prefix}tag <Tag> | create | owner | dump | list | delete | raw | gift | random

  Subcommands:
  - add/create <Name> <Content> (Create a tag)
  - owner <Tag> (Get tag owner)
  - dump [User] (Get list of tag in text file)
  - list [User] | all (Get list of user's tags or all tags)
  - delete/remove <tag> (Delete tag)
  - raw/view <Tag> (View tag's raw source code)
  - random (Get random tag from all tags)
  - gift <Tag> <User> (Gift the tag to someone)

  Base of custom command.
  \`\`\`
`;