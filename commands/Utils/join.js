module.exports = {
	description: 'Set automatic join message when member join the guild',
	cooldown: 10000,
	args: '<Text>',
	guildOnly: true,
	run: async (ctx, { argsString }) => {
		if (!argsString) {
			const sqlResults = await ctx.bot.utils.queryDB('SELECT * FROM guildmember WHERE server = $1', [ctx.guild.id]);
			if (sqlResults.rowCount < 1) return ':x: This guild doesn\'t have JOIN/LEAVE settings set';
			if (!sqlResults.rows[0].joincontent) return ':x: This guild doesn\'t have JOIN message set';
			
			return `Current Join set to <#${sqlResults.rows[0].joinchannelid}> and message:\n\`\`\`${ctx.bot.api.Util.escapeMarkdown(sqlResults.rows[0].joincontent, true, true)}\`\`\``;
		} else if (['off', 'disable'].includes(argsString.toLowerCase())) {
			if (!ctx.member.hasPermission('MANAGE_SERVER')) return ':x: Only member has permission `MANAGE_SERVER` can disable/turn off join message';
			const sqlResults = await ctx.bot.utils.queryDB('SELECT * FROM guildmember WHERE server = $1', [ctx.guild.id]);
			if (sqlResults.rowCount < 1) return ':x: This guild doesn\'t have JOIN message set';
			await ctx.bot.utils.queryDB('UPDATE guildmember SET joincontent = $1, joinchannelid = $2 WHERE server = $3', [null, null, ctx.guild.id]);
			
			return ':white_check_mark: Disable/Remove join messags';
		} else {
			if (!ctx.member.hasPermission('MANAGE_SERVER')) return ':x: Only member has permission `MANAGE_SERVER can set join message';
			const sqlResults = await ctx.bot.utils.queryDB('SELECT * FROM guildmember WHERE server = $1', [ctx.guild.id]);
			if (sqlResults.rowCount < 1) {
				await ctx.bot.utils.queryDB('INSERT  INTO guildmember(server, joincontent, joinchannelid) VALUES ($1, $2, $3)', [ctx.guild.id, argsString, ctx.channel.id])
			} else {
				await ctx.bot.utils.queryDB('UPDATE guildmember SET joincontent = $1, joinchannelid = $2 WHERE server = $3', [argsString, ctx.channel.id, ctx.guild.id]);
			}
			
			return `Set channel <#${ctx.channel.id}> and set Join message set to: \n\`\`\`${ctx.bot.api.Util.escapeMarkdown(argsString, true, true)}\`\`\``;
		}
	}
}
			