const { js_beautify: beautify } = require('js-beautify');

module.exports = {
    description: 'Make shit looks js code to beautify looks',
    category: 'Utils',
    args: {
        type: 'code',
        pattern: '<Code>',
        require: true
    },
    cooldown: 1000,
    run: async function (ctx, { code, lang }) {
        if (!lang) lang = 'js';
        
        if (lang !== 'js') return ctx.bot.messageHandler.invalidArguments(ctx, 'The code must be in Javascript syntax');

        return `\`\`\`js\n${beautify(code)}\`\`\``
    }
}   