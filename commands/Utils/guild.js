const { stripIndent } = require('common-tags')

module.exports = {
    description: 'Get Guild information',
    cooldown: 5000,
    aliases: ['server'],
    guildOnly: true,
    run: async function (ctx) {
        const eStatic = [];
        const eAnimated = [];

        for (let a of ctx.guild.emojis.array()) {
            if (a.animated) {
                eAnimated.push(`<a:${a.name}:${a.id}>`);
            } else {
                eStatic.push(`<:${a.name}:${a.id}>`);
            }
        }

        const embed = new ctx.bot.api.MessageEmbed()
            .setDescription(stripIndent`
                **Guild**     : ${ctx.guild.name}
                **Owner**     : ${ctx.guild.owner.toString()}
                **Region**    : ${ctx.guild.region}
                **Users**     : ${ctx.guild.members.filter(a => !a.user.bot).array().length}
                **Bots**      : ${ctx.guild.members.filter(a => a.user.bot).array().length}
                **Channels**  : ${ctx.guild.channels.filter(a => a.type !== 'category').array().length}
                **Emojis(S)** : ${eStatic.length > 10 ? `${eStatic.slice(0, 11).join(' ')} **${eStatic.length - 10} more..**` : eStatic.join(' ')}
                **Emojis(A)** : ${eAnimated.length > 10 ? `${eAnimated.slice(0, 11).join(' ')} **${eAnimated.length - 10 } more..**` : eAnimated.join(' ')} 
            `)
            .setFooter('monoxbot.ga', ctx.main.user.displayAvatarURL())
            .setTimestamp();

        return embed;
    }
}