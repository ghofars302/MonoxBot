module.exports = {
    description: 'Evaluate system command line.',
    category: 'adminOnly',
    adminOnly: true,
    run: async function (ctx, { argsString }) {
        if (!argsString) return `\`\`\`${ctx.prefix}exec <command line>\n\nEvaluate system command line.\`\`\``;

        const { execSync } = this.childProcess;

        try {
            return `:white_check_mark: ChildProcess results \`\`\`${execSync(argsString)}\`\`\``;
        } catch (error) {
            return `:x: Execute failed.\`\`\`${error}\`\`\``;
        }
    }
}