const bufferType = require('buffer-type')

module.exports = {
	description: 'Evaluates Javascript code on current shard',
	category: 'adminOnly',
	args: {
		type: 'code',
		pattern: '<Javascript code>',
		require: true
	},
	adminOnly: true,
	run: async function (ctx, { code, lang }) {
		const now = Date.now();

		if (!lang) lang = 'js';

		try {
			const result = await eval(`(async()=>{${code}})()`);

			if (Buffer.isBuffer(result)) {
				return ctx.reply(`Took: \`\`${Math.floor(Date.now() - now)}ms\`\`, Output type: \`\`${bufferType(result) ? bufferType(result).type : 'Unknown'}\`\``, {files: 
					[{
						name: bufferType(result) ? (bufferType(result).type === 'image/gif' ? 'image.gif' : bufferType(result).type === 'image/jpeg' ? 'image.jpeg' : bufferType(result).type === 'image/webp' ? 'image.webp' : 'image.png') : 'unknown.png',
						attachment: result
					}]
				})
			}

			return `Took: \`\`${Math.floor(Date.now() - now)}ms\`\`, Output type: \`\`${typeof result}\`\` \`\`\`${lang}\n${ctx.bot.api.Util.escapeMarkdown(await ctx.bot.utils.clean(result), true, true)}\`\`\``
		} catch (error) { 
			ctx.error = true;
			return `Took: \`\`${Math.floor(Date.now() - now)}ms\`\`, Output type: \`\`${error === undefined ? 'undefined' : error === null ? 'null' : error.name}\`\` \`\`\`${lang}\n${typeof error === 'string' ? ctx.bot.api.Util.escapeMarkdown(error, true, true) : error}\`\`\``
		}
	}
}
