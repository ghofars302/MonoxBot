module.exports = {
    description: 'Run a postgres query',
    adminOnly: true,
    args: {
        type: 'code',
        require: true,
    },
    run: async function (ctx, { code }) {
        try {
            const results = await ctx.bot.utils.queryDB(code);

            return `Postgres returned ${results.rowCount} rows \`\`\`js\n${JSON.stringify(results.rows, null, 4)}\`\`\``;
        } catch (error) {
            return `Postgres returned Error \`\`\`js\n${error}\`\`\``;
        }
    }
}