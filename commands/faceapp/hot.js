module.exports = {
    description: 'Female2',
    category: 'Faceapp',
    args: '<@Mentions | User | URL>',
    cooldown: 2500,
    run: async function (ctx, { args }) {
        const images = await ctx.bot.utils.getImagesFromMessage(ctx.message, args);

        if (images.length === 0) return `\`\`\`${ctx.prefix}female2 <@Mentions | User | URL>\n\nFemale2\`\`\``;

        try {
            const buffer = await ctx.bot.fAPI('faceapp/female2', {
                images: images
            })

            return ctx.reply({
                files: [{
                    name: 'female2.jpg',
                    attachment: buffer
                }]
            })
        } catch (error) {
            if (error instanceof ctx.bot.fetch.FetchError) return ':warning: ``API down or took too long``'
            return `:x: \`${error.message}\``
        }
    }
}