module.exports = {
    description: 'Create a US presidential emergency warning with your text',
    args: '<Text>',
    cooldown: 5000,
    aliases: ['alert'],
    run: async function (ctx, { argsString }) {
        if (argsString.length < 2) {
        	if (argsString.length >= 1) return ctx.bot.messageHandler.invalidArguments(ctx, 'The text length must be >= 2');
        	return ctx.bot.messageHandler.invalidArguments(ctx, 'You must put a text');
        }

        const res = await ctx.bot.fAPI('presidential', {
            args: {
            	text: argsString
            }
        });

        return ctx.reply({
            files: [{
                name: 'presidential.png',
                attachment: res
            }]
        })
    }
}