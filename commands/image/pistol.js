module.exports = {
    description: 'Add Gordon Freeman holding Half-life 2 Pistol image in a image',
    category: 'Image',
    cooldown: 5000,
    args: '<@Mentions | User | URL>',
    run: async (ctx, { args }) => {
        const images = await ctx.bot.utils.getImagesFromMessage(ctx.message, args);

        if (images.length === 0) {
            if (['^'].includes(args[0]) || args.length === 0) {
                return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from recent messages');
            }

            return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from the query');
        }

        const buffer = await ctx.bot.fAPI('pistol', {
            images
        });

        return ctx.reply({
            files: [{
                name: 'pistol.png',
                attachment: buffer
            }]
        })
    }
}