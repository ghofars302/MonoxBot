module.exports = {
    description: 'RTX thing...',
    args: '<@Mentions | User | URL>',
    cooldown: 5000,
    run: async function (ctx, { args }) {
        const images = await ctx.bot.utils.getImagesFromMessage(ctx.message, args);
        
        if (images.length === 0) {
            if (['^'].includes(args[0]) || args.length === 0) {
                return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from recent messages');
            }

            return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from the query');
        }
        
        if (images.length === 1) return ctx.bot.messagHandler.InvalidArguments(ctx, 'This command require two Images');

        const res = await ctx.bot.fAPI('rtx', {
            images,
        });

        return ctx.reply({
            files: [{
                name: 'rtx.png',
                attachment: res
            }]
        })
    }
}