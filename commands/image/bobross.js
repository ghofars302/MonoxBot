module.exports = {
    description: 'Put a image on bobross\'s canvas',
    category: 'Image',
    cooldown: 5000,
    args: '<@Mentions | User | URL>',
    aliases: ['paint'],
    run: async (ctx, { args }) => {
        const images = await ctx.bot.utils.getImagesFromMessage(ctx.message, args);

        if (images.length === 0) {
            if (['^'].includes(args[0]) || args.length === 0) {
                return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from recent messages');
            }

            return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from the query');
        }

        const buffer = await ctx.bot.fAPI('bobross', {
            images
        });

        return ctx.reply({
            files: [{
                name: 'bobross.png',
                attachment: buffer
            }]
        })
    }
}