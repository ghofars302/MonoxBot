module.exports = {
    description: 'Glitch the images.',
    category: 'Image',
    cooldown: 5000,
    args: '<@Mentions | User | URL> [Number=15]',
    run: async (ctx, { args }) => {
        let number = [ 15 ];

        const images = await ctx.bot.utils.getImagesFromMessage(ctx.message, args);

        if (images.length === 0) {
            if (['^'].includes(args[0]) || args.length === 0) {
                return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from recent messages');
            }

            return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from the query');
        }

        if (args[1] && !isNaN(args[1])) {
            let num = parseInt(args[1]);

            if (num < 1 || num > 100) return ':x: `Iterations must be > 1 or < 100`';

            number = [ num ]
        }

        const buffer = await ctx.bot.fAPI('glitch', {
            images,
            args: {
            	amount: 15,
                iterations: number
            }
        });

        return ctx.reply(`Iterations: \`${number[0]}\`, amount: \`15\``, {
            files: [{
                name: 'glitch.png',
                attachment: buffer
            }]
        })
    }
}