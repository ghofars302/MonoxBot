module.exports = {
    description: 'Compress image to JPEG format.',
    args: '<@Mentions | User | URL>',
    aliases: ['jpg', 'needsmorejpeg'],
    cooldown: 5000,
    run: async function (ctx, { args }) {
        const images = await ctx.bot.utils.getImagesFromMessage(ctx.message, args);
        
        if (images.length === 0) {
            if (['^'].includes(args[0]) || args.length === 0) {
                return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from recent messages');
            }

            return ctx.bot.messageHandler.invalidArguments(ctx, 'Can\'t find image from the query');
        }

        const res = await ctx.bot.fAPI('magik_script', {
            images,
            args: {
                text: 'jpeg',
                options: [1]
            }
        });

        return ctx.reply({
            files: [{
                name: 'needsmorejpeg.jpg',
                attachment: res
            }]
        })
    }
}