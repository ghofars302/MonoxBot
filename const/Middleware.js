class Middleware {
    constructor(main) {
        this.main = main;
    }

    verify(context, command) {
        const missing = [];

        if (context.author.bot) {
            return { pass: false, missing: missing };
        }

        if (context.main.utils.isAdmin(context.author.id)) {
            return { pass: true, missing: missing };
        }

        if (command.hasPermission.length < 1) {
            return { pass: true, missing: missing }
        }

        for (const perms of command.hasPermission) {
            if (!context.member.hasPermission(perms)) missing.push(perms);
        }

        if (missing.length > 0) {
            return { pass: false, missing: missing}
        } else {
            return { pass: true, missing: missing}
        }
    }
}

module.exports = Middleware;