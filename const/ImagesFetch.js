const axios = require('axios');

class ImagesFetch {
    constructor(main) {
        this.main = main;

        this.regex = {
            emojiRegex: RegExp('[\u{1f300}-\u{1f5ff}\u{1f900}-\u{1f9ff}\u{1f600}-\u{1f64f}\u{1f680}-\u{1f6ff}\u{2600}-\u{26ff}\u{2700}-\u{27bf}\u{1f1e6}-\u{1f1ff}\u{1f191}-\u{1f251}\u{1f004}\u{1f0cf}\u{1f170}-\u{1f171}\u{1f17e}-\u{1f17f}\u{1f18e}\u{3030}\u{2b50}\u{2b55}\u{2934}-\u{2935}\u{2b05}-\u{2b07}\u{2b1b}-\u{2b1c}\u{3297}\u{3299}\u{303d}\u{00a9}\u{00ae}\u{2122}\u{23f3}\u{24c2}\u{23e9}-\u{23ef}\u{25b6}\u{23f8}-\u{23fa}]', 'u'),
            customEmojiRegex: RegExp('^(<:\\w+:)?(?<id>\\d{10,})>?$'),
            isURL: require('url-regex')()
        };
        this.urls = {
            customEmojiEndpoint: 'https://cdn.discordapp.com/emojis/',
            emojiEndpoint: 'https://bot.mods.nyc/twemoji/'
        };
        this.UserAgent = [
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36'
        ];
    }

    async get(ctx, args) {
        const message = ctx instanceof this.main.api.Message ? ctx : ctx.message;

        let images = [];

        for (const { url } of message.attachments.values()) images.push(url);

        if (args[0] !== '^') {
            for (const value of args) {
                if (this.regex.isURL.test(value)) images.push(value);

                const emojiURL = await this.getEmojiURL(value);

                if (emojiURL) {
                    images.push(emojiURL);
                }

                if (value === 'me') {
                    images.push(message.author.displayAvatarURL({
                        size: 2048,
                        format: 'png'
                    }));
                } else if (!message.guild) {
                    if (message.author.tag.toLowerCase().includes(value.toLowerCase() || message.author.id === value.replace(/[^\d]/g, ''))) {
                        images.push(message.author.displayAvatarURL({
                            size: 2048,
                            format: 'png'
                        }))
                    }
                } else if (message.guild) {
                    const match = this.main.utils.getMemberFromString(message, value);

                    if (match) images.push(match.user.displayAvatarURL({
                        size: 2048,
                        format: 'png'
                    }))
                }
            }
        }

        if (images.length === 0 && args.length === 0 || args[0] === '^' && images.length === 0) {
            const messages = await message.channel.messages.fetch({
                limit: 20
            });

            const messageAttachments = messages.filter(m => m.attachments.size > 0 && m.attachments.first().height && m.attachments.first().width);
            const linkEmbeds = messages.filter(m => m.embeds.length > 0 && m.embeds[0].type === 'image');
            const messageEmbeds = messages.filter(m => m.embeds.length > 0 && m.embeds[0].image);
            let imagesMsg = [];

            for (const messageAttachment of messageAttachments.array()) imagesMsg.push({
                url: messageAttachment.attachments.first().url,
                createTimestamp: messageAttachment.createTimestamp
            });

            for (const linkEmbed of linkEmbeds.array()) imagesMsg.push({
                url: linkEmbed.embeds[0].url,
                createTimestamp: linkEmbed.createTimestamp
            });

            for (const messageEmbed of messageEmbeds.array()) imagesMsg.push({
                url: messageEmbed.embeds[0].image.url,
                createTimestamp: messageEmbed.createTimestamp
            });

            imagesMsg = imagesMsg.sort((m1, m2) => m2.createTimestamp - m1.createTimestamp);
            images = imagesMsg.map(i => i.url);
        }

        for (const value of images) {
            if (!await this.isImage(value)) images.remove(value);
        }

        return images;
    }

    async isImage(url) {
        let contentType = ['image/gif', 'image/jpeg', 'image/webp', 'image/png'];

        let httpResponse;

        try {
            httpResponse = await axios({
                method: 'head',
                url,
                headers: {
                    'User-Agent': this.UserAgent.random()
                },
                maxRedirect: 5
            })
        } catch (error) {
            return false;
        }

        if (httpResponse.status !== 200) return false;

        if (contentType.includes(httpResponse.headers['content-type'])) {
            return true;
        } else {
            return false;
        }
    }

    async getEmojiURL(unicode) {
        const isEmoji = this.regex.emojiRegex.test(unicode);
        const customEmojiResult = this.regex.customEmojiRegex.exec(unicode);

        let result;

        if (!isEmoji && !customEmojiResult) return false;

        if (isEmoji) {
            result = `${this.urls.emojiEndpoint}${this.emojiToCodePoint(unicode)}.png`;
        } else {
            result = `${this.urls.customEmojiEndpoint}${customEmojiResult[2]}`;
        }

        if (!await this.isImage(result)) return false;

        return result;
    }

    emojiToCodePoint(unicodeSurrogates) {
        const r = [];
        let c = 0;
        let p = 0;
        let i = 0; {

            while (i < unicodeSurrogates.length) {
                c = unicodeSurrogates.charCodeAt(i++);
                if (p) {
                    r.push((0x10000 + ((p - 0xD800) << 10) + (c - 0xDC00)).toString(16)); // eslint-disable-line no-bitwise
                    p = 0;
                } else if (c >= 0xD800 && c <= 0xDBFF) {
                    p = c;
                } else {
                    r.push(c.toString(16));
                }
            }
            return r.join('-');
        }
    }
}

module.exports = ImagesFetch;