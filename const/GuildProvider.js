class GuildProvider {
    constructor(client, guild) {
        this.client = client;
        this.guild = guild;
    }

    async getPrefix() {
        const prefixResult = await this.client.bot.utils.queryDB('SELECT value FROM settings WHERE setting = $1 AND server = $2', ['prefix', this.guild.id]);

        return prefixResult.rowCount > 0 ? prefixResult.rows[0].value : null;
    }

    async prefix(value, type) {
        try {
            await this.client.bot.utils.queryDB('UPDATE settings SET value = $1 WHERE server = $2', [null, this.guild.id])
            
            if (type === 'set') await this.client.bot.utils.queryDB('UPDATE settings SET value = $1 WHERE server = $2', [value, this.guild.id])
        } catch (error) {
            throw error
        }
    }
}

module.exports = GuildProvider;