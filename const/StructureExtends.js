const {Structures} = require('discord.js');
const Provider = require('./GuildProvider');
const Promise = require('bluebird');

module.exports = Structures.extend('Guild', Guild => {
    class MonoxBotGuild extends Guild {
        constructor(...args) {
            super(...args);

            this.provider = new Provider(this.client, this)

            this.guildPrefix = 'm!';
            this.initPrefix();
        }

        async initPrefix() {
            try {
                const result = await this.provider.getPrefix();

                this.guildPrefix = 'm!';

                return true
            } catch (error) {
                throw error;
            }
        }
        
        get commandPrefix() {
            if (this.guildPrefix === null) return this.client.prefix;
            return this.guildPrefix;
        }

        set commandPrefix(value) {
            return new Promise(async (resolve, reject) => {
                try {
                    await this.provider.prefix(value, 'set');
                    resolve();
                } catch (error) {
                    reject(error)
                }
            })
        }
    }

    return MonoxBotGuild;
})

const a = Structures.extend('Message', Message => {
    class Context extends Message {
        constructor(...args) {
            super(...args);
            
            this.prefix = this.guild ? this.guild.commandPrefix : this.client.prefix;
            this.commandEdits = [];
        }
        
		reply(...args) {
        	return this.sendFunction(args);
        }
        
        direct(...args) {
        	return this.sendFunction(args, 'dm');
        }
        
        async sendFunction(args, type) {
        	if (this.deleted) {
        		return;
        	}
        
        	if (args.length !== 0 && typeof args[0] === 'string' && (args[0].includes(context.main.token))) {
          	  this.bot.logger.log(this.bot.client, `[WARNING] A message tried to send BOT TOKEN`); // eslint-disable-line no-console
            	args[0] = args[0].replace(new RegExp(context.main.token, 'g'), '<TOKEN_CENSORED>');
       	 }
       
       	let msgObject;
       
       	 if (type === 'dm') {
       		msgObject = await this.author.send(...args);
       		this.commandEdits.push(msgObject);
       	} else {
       		msgObject = await this.channel.send(...args)/
       		this.commandEdits.push(msgObject);
       	}
       
       	return msgObject;
       }
        
        get main() {
        	return this.client;
        }
        
        get splitContent() {
        	return this.content.split(/ +/g);
        }
        
        get bot() {
        	return this.client.bot;
        }
        
        get message() {
        	return this;
        }
        
        get users() {
        	return this.client.users;
        }
        
        get isDM() {
        	return !this.guild;
        }
        
        get isGuild() {
        	return !!this.guild;
        }
    }

    return Context;
});

module.exports = Structures.extend('TextChannel', TextChannel => {
    class GuildTextChannel extends TextChannel {
        constructor(...args) {
            super(...args);
        }
    }

    return GuildTextChannel;
})
