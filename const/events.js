const Parser = require('./Parser');
const BufferType = require('buffer-type');

class RegisterEvent {
    constructor(bot) {
        this.bot = bot;
        this.client = bot.client;

        this.MessageEvents();
        this.BotEvents();
        this.PaginationEvents();
    }

    MessageEvents() {
        this.client.on('message', async (msg) => {
            const context = await this.bot.context.initContext(msg);

            return this.bot.messageHandler.ContextCommand(context);
        });

        this.client.on('messageUpdate', async (oldMsg, newMsg) => {
            if (oldMsg.content === newMsg.content) return;

            try {
                if (this.bot.commandEditingStore.has(oldMsg.id)) {
                    const lastMessage = this.bot.commandEditingStore.get(oldMsg.id);
                    if (lastMessage) {
                        lastMessage.delete();
                        this.bot.commandEditingStore.delete(oldMsg.id);
                        if (oldMsg.timer) {
                            clearTimeout(oldMsg.timer);
                        }
                    }
                }
            } catch (e) {} // eslint-disable-line no-empty

            const context = await this.bot.context.initContext(newMsg);

            return this.bot.messageHandler.ContextCommand(context);
        });

        this.client.on('messageDelete', (msg) => {
            if (!this.bot.commandEditingStore.has(msg.id)) return;

            try {
                const lastMessage = this.bot.commandEditingStore.get(msg.id);
                if (lastMessage) {
                    lastMessage.delete();
                    this.bot.commandEditingStore.delete(msg.id);
                    if (msg.timer) {
                        clearTimeout(msg.timer);
                    }
                }
            } catch (e) {} // eslint-disable-line no-empty
        });
    }
    
    ContextEvents() {
    	this.client.on('message', async ctx => {
    		return this.bot.messageHandler.ContextCommand(ctx);
    	});
    
    	this.client.on('messageUpdate', async (oldCtx, newCtx) => {
    		if (oldCtx.content === newCtx.content) return;
    
    		for (const i of oldCtx.commandEdits) {
    			try {
    				if (i.deletable) await i.delete();
    			} catch(error) {}
    		}
    
    		return this.bot.messageHandler.ContextCommand(newCtx);
    	});
    
    	this.client.on('messageDelete', async ctx => {
    		for (const i of ctx.commandEdits) {
    			try {
    				if (i.deletable) await i.delete();
    			} catch (error) {}
    		}
    	});
    }

    BotEvents() {
        this.client.on('error', (err) => {
            const ErrCleaned = this.bot.util.inspect(err);
            this.bot.logger.error(`[ERROR] Error websocked: ${ErrCleaned}`);
        });

        this.client.on('disconnect', () => {
            this.bot.logger.error(`[DISCONNECT] Disconnect from discord API.`);
            process.exit();
        });

        this.client.on('ready', async () => {
            if (!this.client.user.bot) {
                this.bot.logger.error(`[TOKEN] Token was user's token and MonoxBot not support selfbot mode, exiting...`);
                return process.exit();
            }

            const fetched = await this.client.fetchApplication();

            if (this.client.owner === "" || !this.client.owner) {
                this.client.owner = fetched.owner.id;
                this.bot.config.admins = fetched.owner.id;
            }

            if (!this.client.shard) {
                this.bot.logger('Running without sharding cause some commands not working.')
            }

            this.client.secret = fetched.botPublic ? false : true; // eslint-disable-line no-unneeded-ternary

            this.bot.logger.log(this.client, `Ready with guilds ${this.client.guilds.size}`);
            this.client.user.setActivity(`Bot database currently offline, some commands unavailable`, {
                type: 'PLAYING'
            });
        });

        this.client.on('ratelimit', (ratelimit) => {
            this.bot.logger.error(`[Shard ${this.client.shard.id}] [Ratelimit] ${ratelimit.info} at ${ratelimit.path}`)
        })
    }

    PaginationEvents() {
        this.client.on('messageReactionAdd', (react, user) => {
            this.bot.Paginate.handleReactionAdd(react, user);
        })

        this.client.on('messageReactionRemove', (react, user) => {
            this.bot.Paginate.handleReactionRemove(react, user);
        })
    }

    GuildEvents() {
        this.client.on('guildMemberAdd', async member => {
            const sqlResults = await this.bot.utils.queryDB('SELECT * FROM guildmember WHERE server = $1', [member.guild.id]);
            if (sqlResults.rowCount < 1) return;

            const channel = this.client.channels.get(sqlResults.rows[0].joinchannelid);
            if (!channel) return;
            const Parsed = await new Parser(this.client, {
                bot: this.bot,
                main: this.client,
                member: member,
                guild: member.guild,
                prefix: (member.guild.commandPrefix || this.client.prefix),
                members: member.guild.members,
                message: {
                	channel: channel,
                	member: member,
                	guild: member.guild
                }
            }).parse(sqlResults.rows[0].joincontent, [], {});

            let text = Parsed.result;
            const attachments = Parsed.attachments;
            const imagescripts = Parsed.imagescripts;

            let Files = [];

            let isError = Parsed.error;
            let count = 0;

            for (const i of attachments) {
                if (isError) continue;

                try {
                    const buffer = await this.bot.fetch(i.url).then(a => a.buffer());
                    Files.push(new this.bot.api.MessageAttachment(buffer, i.name ? i.name : (BufferType(buffer).type === 'image/gif' ? `image${count}.gif` : `image${count}.png`)));
                    ++count;
                } catch (error) {
                    isError = error;
                }
            }

            for (const i of imagescripts) {
                if (isError) continue;

                try {
                    const buffer = await this.bot.fAPI('parse_tag', {
                        args: {
                            text: i.script
                        }
                    });
                    Files.push(new this.bot.api.MessageAttachment(buffer, i.name ? i.name : (BufferType(buffer).type === 'image/gif' ? `image${count}.gif` : `image${count}.png`)));
                    ++count;
                } catch (error) {
                    isError = error;
                }
            }

            if (isError) {
                Files = [];
                text = `:warning: ${isError.message}`;
            }

            if (text.length < 1 && Files.lenght < 1) text = 'This message was send because MemberAddEvent was set but the result was empty or nothing';

            try {
                await channel.send(text, Files)
                return;
            } catch (error) {
                return;
            }
        });

        this.client.on('guildMemberRemove', async member => {
            const sqlResults = await this.bot.utils.queryDB('SELECT * FROM guildmember WHERE server = $1', [member.guild.id]);
            if (sqlResults.rowCount < 1) return;

            const channel = this.client.channels.get(sqlResults.rows[0].leavechannelid);
            if (!channel) return;
            const Parsed = await new Parser(this.client, {
                bot: this.bot,
                main: this.client,
                guild: member.guild,
                member: member,
                members: member.guild.member,
                prefix: (member.guild.commandPrefix || this.client.prefix),
                message: {
                	channel: channel,
                	member: member,
                	guild: member.guild
                }
            }).parse(sqlResults.rows[0].leavecontent, [], {});

            let text = Parsed.result;
            const attachments = Parsed.attachments;
            const imagescripts = Parsed.imagescripts;

            let Files = [];

            let isError = Parsed.error;
            let count = 0;

            for (const i of attachments) {
                if (isError) continue;

                try {
                    const buffer = await this.bot.fetch(i.url).then(a => a.buffer());
                    Files.push(new this.bot.api.MessageAttachment(buffer, i.name ? i.name : (BufferType(buffer).type === 'image/gif' ? `image${count}.gif` : `image${count}.png`)));
                    ++count;
                } catch (error) {
                    isError = error;
                }
            }

            for (const i of imagescripts) {
                if (isError) continue;

                try {
                    const buffer = await this.bot.fAPI('parse_tag', {
                        args: {
                            text: i.script
                        }
                    });
                    Files.push(new this.bot.api.MessageAttachment(buffer, i.name ? i.name : (BufferType(buffer).type === 'image/gif' ? `image${count}.gif` : `image${count}.png`)));
                    ++count;
                } catch (error) {
                    isError = error;
                }
            }

            if (isError) {
                Files = [];
                text = `:warning: ${isError.message}`;
            }

            if (text.length < 1 && Files.lenght < 1) text = 'This message was send because MemberRemoveEvent was set but the result was empty or nothing';

            try {
                await channel.send(text, Files)
                return;
            } catch (error) {
            	console.log(error);
                return;
            }
        });
    }
}

module.exports = RegisterEvent;