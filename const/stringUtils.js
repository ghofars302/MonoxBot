class stringUtils {
    constructor(main) {
        this.main = main;
    }

    displayHelpPages(nsfw, pageNumber) {
    	let collec = nsfw ? this.main.allHelpPages : this.main.helpPages
    
        pageNumber = pageNumber || 1;

        if (pageNumber < 1 || pageNumber > collec.length) pageNumber = 1;

        let output = `- MonoxBot command list (Page ${pageNumber} of ${collec.length})`;

        output += `\`\`\`${collec[pageNumber - 1]}\`\`\``;
        if (!nsfw) output += '*Some commands are hidden if in Safe At Work channels*\n';
        output += `To see individual commands, type help <command>\n`;
        output += `To paginate the Help command react this message`;

        return output;
    }
}

module.exports = stringUtils;