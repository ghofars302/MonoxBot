/*
* fBot tag parser
* Credited to https://gitlab.com/matmen
*
* Thanks lol
*/

const fetch = require('node-fetch');
const discord = require('discord.js');
const fAPI = require('../modules/fAPI');

const preParseTags = ['ignore', 'note'];
const postParseTags = ['attach', 'iscript'];

const rexLangs = ['c#', 'visualbasic', 'vb', 'f#', 'java', 'python2', 'python2.7', 'py2.7', 'py2', 'c(gcc)', 'c', 'c++(gcc)', 'c++', 'cpp', 'php', 'php7', 'pascal', 'objective-c', 'oc', 'haskell', 'hs', 'ruby', 'rb', 'perl', 'lua', 'assembly', 'asm', 'sqlserver', 'javascript', 'js', 'commonlisp', 'lisp', 'prolog', 'go', 'scala', 'scheme', 'node.js', 'node', 'python', 'python3', 'py', 'py3', 'octave', 'c(clang)', 'c++(clang)', 'c++(vc++)', 'c(vc)', 'd', 'r', 'tcl', 'mysql', 'postgresql', 'psql', 'postgres', 'oracle', 'swift', 'bash', 'ada', 'erlang', 'elixir', 'ex', 'ocaml', 'kotlin', 'kot', 'brainfuck', 'bf', 'fortran', 'fort'];

class Parser {
	constructor(client, context) {
		this.client = client;
		this.context = context;
		this.apiConnector = fAPI;

		this.stackSize = 0;
		this.rexCalls = 0;
		this.hasteCalls = 0;

		this.attachments = [];
		this.imagescripts = [];

		this.nsfw = false;

		this.variables = new Map();
	}

	async parse(input, tagArgs, tag) {
		try {
			const result = await this.subParse(input, tagArgs, tag, false, true);
			const parsed = Parser.unescapeTag(result).replace(/\\{/g, '{').replace(/\\}/g, '}');
			if (parsed.length > 2000) return { success: false, nsfw: false, attachments: [], imagescripts: [], result: `:warning: Results exceeds size limits (2000)` };
			return { success: true, nsfw: this.nsfw, attachments: this.attachments, imagescripts: this.imagescripts, result: Parser.unescapeTag(result).replace(/\\{/g, '{').replace(/\\}/g, '}') };
		} catch (err) {
			return { success: false, nsfw: false, attachments: [], imagescripts: [], result: `:warning: ${err.stack}` };
		}
	}

	async subParse(input, tagArgs, tag, filter, initial, help) {
		this.stackSize++;
		if (this.stackSize > 1000)
			throw new Error(`Stack size exceeded at: \`${input}\``);

		if (initial) input = await this.subParse(input, tagArgs, tag, preParseTags);

		let tagEnd, tagStart;

		for (let i = 0; i < input.length; i++) {

			if (input[i] === '}' && (input[i + 1] !== '\\' && input[i - 1] !== '\0')) {
				tagEnd = i;

				for (let e = tagEnd; e >= 0; e--) {
					if (input[e] === '{' && (input[i - 1] !== '\\' && input[e + 1] !== '\0')) {
						tagStart = e + 1;

						const toParse = input.slice(tagStart, tagEnd).trim();

						const split = toParse.split(':');
						const tagName = split.shift();

						if (filter && !filter.includes(tagName)) continue;
						if (!filter && postParseTags.includes(tagName)) continue;

						const rawArgs = split.join(':').replace(/\\\|/g, '\0|');
						const args = [];

						let currentArg = '';
						for (let i = 0; i < rawArgs.length; i++) {
							if (rawArgs[i] === '|' && rawArgs[i - 1] !== '\0') {
								args.push(currentArg);
								currentArg = '';
							} else currentArg += rawArgs[i];
						}

						if (currentArg) args.push(currentArg);

						const before = input.substring(0, tagStart - 1);
						const after = input.substring(tagEnd + 1, input.length);

						const tagResult = Parser.escapeTag(((await this.getData(tagName, rawArgs, args, tagArgs, tag, help)) || '').toString());

						input = before + tagResult + after;
						i = before.length + tagResult.length - 1;
						break;
					}
				}
			}
		}

		if (initial) input = await this.subParse(input, tagArgs, tag, postParseTags);

		return input;
	}

	async getData(key, rawArgs, splitArgs, args, tag, help) {
		key = key.trim();
		rawArgs = rawArgs.trim();
		splitArgs = splitArgs.map(arg => arg.trim());

		switch (key) {
			case 'help': {
				if (help) {
					return `\`{${key}}\`

						Return a page of all available key to parse
						Example: \`{${key}:iscript}\`
					
					`
				}

				if (rawArgs) {
					return await this.subParse(`{${rawArgs}}`.replace(/\0/g, ''), [], {}, false, true, true);
				}

				return `--- Discord ---

				\`{user}\` \`{discrm}\` \`{tag}\` \`{id}\`
				\`{image}\` \`{avatar}\` \`{randuser}\` \`{randchannel}\`
				\`{channel}\` \`{server}\` \`{member}\` \`{}\`

				--- Logic ---

				\`{isnumber}\` \`{if}\` \`{then}\` \`{else}\`

				--- Math ---
				\`{math}\`
				
				`
			}

			case 'user':
			case 'username': {
				if (help) return `
					\`{${key}}\`

					Return user's username;
					Example: \`{${key}}\` \`{${key}:Monox} 
				`

				const member = rawArgs ? this.context.guild.getMemberFromString(rawArgs) : (this.context && this.context.member);
				return member ? member.user.username : '';
			}

			case 'discrim':
			case 'discriminator': {
				if (help) return `
					\`{${key}}\`

					Return user's dsicriminator
					Example: \`{${key}}\` \`{${key}:Monox} 
				`

				const member = rawArgs ? this.context.guild.getMemberFromString(rawArgs) : (this.context && this.context.member);
				return member ? member.user.discriminator : '';
			}

			case 'tag':
			case 'mention': {
				if (help) return `
					\`{${key}}\`

					Return user's tag
					Example: \`{${key}}\` \`{${key}:Monox} 
				`

				const member = rawArgs ? this.context.guild.getMemberFromString(rawArgs) : (this.context && this.context.member);
				return member ? member.user.tag : '';
			}

			case 'id':
			case 'userid': {
				if (help) return `
					\`{${key}}\`

					Return user's id
					Example: \`{${key}}\` \`{${key}:Monox} 
				`

				const member = rawArgs ? this.context.guild.getMemberFromString(rawArgs) : (this.context && this.context.member);
				return member && member.user.id;
			}

			case 'avatar': {
				if (help) return `
					\`{${key}}\`

					Return user's avatar
					Example: \`{${key}}\` \`{${key}:Monox} 
				`

				const member = rawArgs ? this.context.guild.getMemberFromString(rawArgs) : (this.context && this.context.member);
				return member ? member.user.displayAvatarURL({
					format: (member.user.avatar || '').startsWith('a_') ? 'gif' : 'png',
					size: 2048
				}) : '';
			}

			case 'randuser': {
				if (help) return `
					\`{${key}}\`

					Return random user
					Example: \`{${key}}\`
				`

				return this.context && (this.context.guild ? this.context.guild.members.random().user.username : this.context.author.username);
			}

			case 'randchannel': {
				if (help) return `
					\`{${key}}\`

					Return random channel in guild
					Example: \`{${key}}\`
				`

				return this.context && `#${this.context.guild ? this.context.guild.channels.random().name : this.context.author.username}`;
			}

			case 'tagname': {
				if (help) return `
					\`{${key}}\`

					Return tag name.
					Example: \`{${key}}\`
				`

				return tag.name;
			}

			case 'tagowner': {
				if (help) return `
					\`{${key}}\`

					Return tag owner.
					Example: \`{${key}}\`
				`

				return (await this.client.users.fetch(tag.ownerid)).tag;
			}

			case 'dm':
				if (help) return `
					\`{${key}}\`

					Return 1 if DM channel else 0 if not DM channel.
					Example: \`{${key}}\`
				`

				if (!this.context) return 'N/A';
				return this.context.guild ? 0 : 1;

			case 'channels':
				if (help) return `
					\`{${key}}\`

					Return channels size.
					Example: \`{${key}}\`
				`

				if (!this.context || !this.context.guild) return 'N/A';
				return this.context.guild.channels.size;

			case 'members':
			case 'servercount':
				if (help) return `
					\`{${key}}\`

					Return membercount
					Example: \`{${key}}\`
				`

				if (!this.context || !this.context.guild) return 'N/A';
				return this.context.guild.memberCount;

			case 'messageid':
				if (help) return `
					\`{${key}}\`

					Return your message id.
					Example: \`{${key}}\`
				`

				if (!this.context) return 'N/A';
				return this.context.id;

			case 'owner':
				if (help) return `
					\`{${key}}\`

					Return server owner tag.
					Example: \`{${key}}\`
				`

				if (!this.context || !this.context.guild) return 'N/A';
				return this.context.guild.owner.user.tag;

			case 'serverid':
				if (help) return `
					\`{${key}}\`

					Return server id.
					Example: \`{${key}}\`
				`

				if (!this.context || !this.context.guild) return 'N/A';
				return this.context.guild.id;

			case 'server':
				if (help) return `
					\`{${key}}\`

					Return server name.
					Example: \`{${key}}\`
				`

				if (!this.context || !this.context.guild) return 'N/A';
				return this.context.guild.name;

			case 'channelid':
				if (help) return `
					\`{${key}}\`

					Return channel id
					Example: \`{${key}}\`
				`
			
				if (!this.context) return 'N/A';
				return this.context.channel.id;

			case 'channel':
				if (help) return `
					\`{${key}}\`

					Return channel name.
					Example: \`{${key}}\`
				`

				if (!this.context) return 'N/A';
				return this.context.channel.name;

			case 'nick':
			case 'nickname': {
				if (help) return `
					\`{${key}}\`

					Return member's nickname
					Example: \`{${key}}\` \`{${key}:Monox}\`
				`

				const member = rawArgs ? this.context.guild.getMemberFromString(rawArgs) : (this.context && this.context.member);
				return member && (member.nickname || member.user.username);
			}

			case 'me':
				if (help) return `
					\`{${key}}\`

					Return Bot username.
					Example: \`{${key}}\`
				`

				return this.client.user.username;

			case 'prefix':
				if (help) return `
					\`{${key}}\`

					Return used prefix.
					Example: \`{${key}}\`
				`

				return (this.context.prefix) || 'N/A';

			case 'range': {
				if (help) return `
					\`{${key}}\`

					Return range between number
					Example: \`{${key}:55|44}\`
				`

				const lower = Math.min(...splitArgs) || 0;
				const upper = Math.max(...splitArgs) || 0;
				return Math.round(Math.random() * (upper - lower)) + lower;
			}

			case 'random':
			case 'choose':
				if (help) return `
					\`{${key}}\`

					Return random value.
					Example: \`{${key}:this|or}\`
				`

				return splitArgs[Math.floor(Math.random() * splitArgs.length)];

			case 'select': {
				if (help) return `
					\`{${key}}\`

					Return select from value??
					Example: \`{${key}}\`
				`

				if (isNaN(splitArgs[0])) return;
				const index = parseInt(splitArgs.shift());
				return splitArgs[index];
			}

			case 'set': {
				if (help) return `
					\`{${key}}\`

					Set a value.
					Example: \`{${key}:this|kek}\`

					See also: \`{get}\`
				`

				this.variables.set(splitArgs.shift(), splitArgs.join(':'));
				return;
			}

			case 'get': {
				if (help) return `
					\`{${key}}\`

					Get value from set.
					Example: \`{${key}:this}\`
				`

				return this.variables.get(splitArgs.shift()) || '';
			}

			case 'attach':
			case 'file':
				if (help) return `
					\`{${key}}\`

					Attach a file/image. Note: maximum upload limit was 8MB for Bot.
					Example: \`{${key}}\`
				`

				if (!splitArgs.length) return;
				this.attachments.push({ url: splitArgs[0], name: splitArgs[1] });
				return;

			case 'iscript':
			case 'imagescript':
				if (help) return `
					\`{${key}}\`

					Manipulate image using imagescript
					Example: \`{${key}:screenshot google.com}\`

					See also: https://gitlab.com/snippets/1736663
				`

				if (!splitArgs.length) return 'https://gitlab.com/snippets/1736663';
				this.imagescripts.push({ script: splitArgs[0], name: splitArgs[1] });
				return;

			case 'image':
			case 'lastimage': {
				if (help) return `
					\`{${key}}\`

					Get last image in channel.
					Example: \`{${key}}\`
				`

				try {
					const images = await this.context.bot.utils.getImagesFromMessage(this.context, rawArgs.split(/ +/g));
					return images[0] || '';
				} catch (err) {
					return '';
				}
			}

			case 'note':
				if (help) return `
					\`{${key}}\`

					Set a note, only for in view tag source. removed when parsed.
					Example: \`{${key}:This will removed tho.}\`
				`

				return;

			case 'eval':
				if (help) return `
					\`{${key}}\`

					Parse everything inside.
					Example: \`{${key}:{avatar}}\`
				`

				return await this.subParse(rawArgs.replace(/\0/g, ''), args);

			case 'args':
				if (help) return `
					\`{${key}}\`

					Get args. Note: when using test mode {args} will return random word.
					Example: \`{${key}}\`
				`

				return args.join(' ');

			case 'arg': {
				if (help) return `
					\`{${key}}\`

					Get args. Note: when using test mode {arg} will return random word.
					Example: \`{${key}}\`
				`

				if (isNaN(splitArgs[0])) return;
				const index = parseInt(splitArgs[0]);
				return args[index];
			}

			case 'argslen':
			case 'argslength':
			case 'argscount':
				if (help) return `
					\`{${key}}\`

					Return split args length
					Example: \`{${key}:aaaaaaaa}\`
				`

				return args.length;

			case 'replace': {
				if (help) return `
					\`{${key}}\`

					Replace using text???
					Example: \`{${key}}\`
				`
				const [replace, replacement, text] = splitArgs;
				return (text || '').replace(new RegExp(replace.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'g'), replacement || '');
			}

			case 'replaceregex': {
				if (help) return `
					\`{${key}}\`

					Replace using regex???
					Example: \`{${key}}\`
				`
				const [replace, replacement, text, flags] = splitArgs;
				return (text || '').replace(new RegExp(replace, flags || 'g'), replacement || '');
			}

			case 'upper':
				if (help) return `
					\`{${key}}\`

					Make all text inside to UPPER CASE.
					Example: \`{${key}:lowercasetext}\`
				`

				return rawArgs.toUpperCase();

			case 'lower':
				if (help) return `
					\`{${key}}\`

					Parse everything inside.
					Example: \`{${key}:{avatar}}\`
				`

				return rawArgs.toLowerCase();

			case 'trim':
				if (help) return `
					\`{${key}}\`

					Parse everything inside.
					Example: \`{${key}:{avatar}}\`
				`

				return rawArgs.trim();

			case 'length':
				if (help) return `
					\`{${key}}\`

					Return unsplitArgs length
					Example: \`{${key}:sdfsdfsd}\`
				`

				return rawArgs.length;

			case 'url':
				if (help) return `
					\`{${key}}\`

					Encode string to url format
					Example: \`{${key}:{avatar}}\`
				`
				return encodeURI(rawArgs);

			case 'urlc':
			case 'urlcomponent':
				if (help) return `
					\`{${key}}\`

					Encode string to url format
					Example: \`{${key}:{avatar}}\`
				`
				return encodeURIComponent(rawArgs);

			case 'date':
			case 'time': {
				if (help) return `
					\`{${key}}\`

					Get date/time
					Example: \`{${key}:MMMM|YYYY}\`
				`

				let [format, offset, timeOverride] = splitArgs;
				if (!format) return '';

				offset = offset ? Parser.timespanToMillis(offset) : 0;

				const time = (timeOverride ? parseInt(timeOverride) : Date.now()) + offset;
				const date = new Date(time);

				if (!date.valueOf()) return '';
				if (['unix', 'ms'].includes(format)) return date.valueOf();

				const fullMonth = ['January', 'Feburary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][date.getUTCMonth()];
				const abbrMonth = fullMonth.slice(0, 3);

				const fullDoW = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][date.getUTCDay()];
				const abbrDoW = fullDoW.slice(0, 3);

				let result = '';

				const getReplacement = match => {
					switch (match) {
						case 'CCCC':
							return Math.floor(date.getUTCFullYear() / 100) + 1;
						case 'YYYY':
							return date.getUTCFullYear();
						case 'YY':
							return date.getUTCFullYear().toString().slice(-2);
						case 'MMMM':
							return fullMonth;
						case 'MMM':
							return abbrMonth;
						case 'MM':
							return `${date.getUTCMonth() < 9 ? '0' : ''}${date.getUTCMonth() + 1}`;
						case 'M':
							return date.getUTCMonth() + 1;
						case 'DDDD':
							return fullDoW;
						case 'DDD':
							return abbrDoW;
						case 'DD':
							return `${date.getUTCDate() < 10 ? '0' : ''}${date.getUTCDate()}`;
						case 'D':
							return date.getUTCDate();
						case 'hh':
							return `${date.getUTCHours() < 10 ? '0' : ''}${date.getUTCHours()}`;
						case 'h':
							return date.getUTCHours();
						case 'mm':
							return `${date.getUTCMinutes() < 10 ? '0' : ''}${date.getUTCMinutes()}`;
						case 'm':
							return date.getUTCMinutes();
						case 'ssss':
							return date.getUTCMilliseconds();
						case 'ss':
							return `${date.getUTCSeconds() < 10 ? '0' : ''}${date.getUTCSeconds()}`;
						case 's':
							return date.getUTCSeconds();
						default:
							return match;
					}
				};

				for (const match of format.match(/(C|Y|M|D|h|m|s)\1+|./g))
					result += getReplacement(match);

				return result;
			}

			case 'joined':
			case 'jointime': {
				if (help) return `
					\`{${key}}\`

					Return member's joined time
					Example: \`{${key}}\` \`{${key}:Monox}
				`

				const member = splitArgs[1] ? this.context.main.utils.getMemberFromString(this.context, splitArgs[1]) : (this.context && this.context.member);
				if (!member) return '';

				if (splitArgs[0] === 'discord')
					return discord.SnowflakeUtil.deconstruct(member.user.id).date.valueOf();

				else if (splitArgs[0] === 'server')
					return member.joinedTimestamp;

				else return '';
			}

			case 'abs':
			case 'absolute': {
				if (help) return `
					\`{${key}}\`

					Math abs
					Example: \`{${key}:34242}\`
				`

				if (isNaN(rawArgs)) return;
				return Math.abs(parseFloat(rawArgs));
			}

			case 'pi':
				if (help) return `
					\`{${key}}\`

					Math.pi
					Example: \`{${key}}\`
				`

				return Math.PI;

			case 'e':
				if (help) return `
					\`{${key}}\`

					Math.E
					Example: \`{${key}:12345}\`
				`

				return Math.E;

			case 'min':
				if (help) return `
					\`{${key}}\`

					Minimum number in args
					Example: \`{${key}:12345}\`
				`

				return Math.min(...splitArgs);

			case 'max':
				if (help) return `
					\`{${key}}\`

					Maximum number in args
					Example: \`{${key}:12345}\`
				`

				return Math.max(...splitArgs);

			case 'round':
				if (help) return `
					\`{${key}}\`

					Round number.
					Example: \`{${key}:0.312312423423}\`
				`

				return Math.round(rawArgs);

			case 'ceil':
				if (help) return `
					\`{${key}}\`

					Math ceil
					Example: \`{${key}:423432423}\`
				`

				return Math.ceil(rawArgs);

			case 'floor':
				if (help) return `
					\`{${key}}\`

					Math floor
					Example: \`{${key}:44455}\`
				`

				return Math.floor(rawArgs);

			case 'sign':
				if (help) return `
					\`{${key}}\`

					Math sign
					Example: \`{${key}:34566}\`
				`

				return Math.sign(rawArgs);

			case 'sin':
				if (help) return `
					\`{${key}}\`

					Math sin
					Example: \`{${key}:34566}\`
				`
				return Math.sin(rawArgs);

			case 'cos':
				if (help) return `
					\`{${key}}\`

					Math cos
					Example: \`{${key}:34566}\`
				`
				
				return Math.cos(rawArgs);

			case 'tan':
				if (help) return `
					\`{${key}}\`

					Math tan
					Example: \`{${key}:34566}\`
				`
				
				return Math.tan(rawArgs);

			case 'sqrt':
				if (help) return `
					\`{${key}}\`

					Math sqrt
					Example: \`{${key}:34566}\`
				`

				return Math.sqrt(rawArgs);

			case 'root': {
				if (help) return `
					\`{${key}}\`

					idk this root
					Example: \`{${key}:34566}\`
				`

				if (isNaN(splitArgs[0])) return;
				if (isNaN(splitArgs[1])) return;

				const root = parseFloat(splitArgs.shift());
				const num = parseFloat(splitArgs.shift());

				return num ** (1 / root);
			}

			case 'math': {
				if (help) return `
					\`{${key}}\`

					Math, yeah math (I hate math)
					Example: \`{${key}:565|-|4554}\`
				`

				if (!isNaN(rawArgs)) return parseFloat(rawArgs).toString();

				if (/[^+\-*^/()0-9.% ]/g.test(rawArgs)) throw new Error(`Invalid term \`${rawArgs}\``);
				try {
					return eval(rawArgs.replace(/\^/g, '**').replace(/([^\d]?)0(\d+)/g, (match, b, a) => b + a)).toString();
				} catch (err) {
					throw new Error(`Failed to calculate term \`${rawArgs}\``);
				}
			}

			case 'if': {
				if (help) return `
					\`{${key}}\`

					Set a condition
					Example: \`{${key}:{avatar}|=|{ignore:{avatar}}}\`

					See also: \`{else}\` \`{then}\`
				`

				const value = splitArgs.shift();
				const operator = splitArgs.shift();
				const compareValue = splitArgs.shift();
				let onMatch, onNoMatch;

				for (const part of splitArgs) {
					const splitPart = part.split(':');
					const action = splitPart.shift();
					if (action === 'then') throw new Error('then/else inside if statement are shit af use `{then}` or `{else}` instead')
					if (action === 'else') throw new Error('then/else inside if statement are shit af use `{then}` or `{else}` instead')
				}

				const result = Parser.compareLogic(value, compareValue, operator);
				this.lastIfResult = result;
				return result ? onMatch : onNoMatch;
			}
			
			case 'isnumber': {
				if (help) return `
					\`{${key}}\`
					
					Return true if value was number, else false
					Example: \`{${key}:44}\`
					
					See also: \`{else}\` \`{then}\`
				`
				this.lastIfResult = !Number.isNaN(rawArgs)
				return '';
			}

			case 'then':
				if (help) return `
					\`{${key}}\`

					Do something if statement return true.
					Example: \`{${key}:aaaaa}\`

					See also: \`{if}\` \`{else}\`
				`

				if (this.lastIfResult === undefined) return '';
				return this.lastIfResult ? rawArgs : '';

			case 'else':
				if (help) return `
					\`{${key}}\`

					Do something if statement return false
					Example: \`{${key}:ok}\`

					See also: \`{if}\` \`{then}\`
				`

				if (this.lastIfResult === undefined) return '';
				return this.lastIfResult ? '' : rawArgs;

			case 'codeblock':
				if (help) return `
					\`{${key}}\`

					warp text with code block
					Example: \`{${key}:34566}\`
				`

				return `\`\`\`${rawArgs.replace(/`/g, '`\u200b')}\`\`\``;

			case 'code':
				if (help) return `
					\`{${key}}\`

					warp text with code????
					Example: \`{${key}:34566}\`
				`

				return `\`${rawArgs.replace(/`/g, '')}\``;

			case 'bold':
				if (help) return `
					\`{${key}}\`

					Bold the text
					Example: \`{${key}:34566}\`
				`

				return `**${rawArgs.replace(/\*{2}/g, '')}**`;

			case 'strikethrough':
				if (help) return `
					\`{${key}}\`

					Strike the text to ~~EXAMPLE~~
					Example: \`{${key}:34566}\`
				`

				return `~~${rawArgs.replace(/~/g, '')}~~`;

			case 'limit':
				if (help) return `
					\`{${key}}\`

					idk how i describe this.
					Example: \`{${key}:34566|555}\`
				`

				return (splitArgs[0] || '').slice(0, parseInt(splitArgs[1]) || 2000);

			case 'ignore':
				if (help) return `
					\`{${key}}\`

					Make this unparsed.
					Example: \`{${key}:34566}\`
				`

				return Parser.escapeTag(rawArgs);

			case 'nsfw':
				if (help) return `
					\`{${key}}\`

					Make tag marked as NSFW
					Example: \`{${key}}\` \`{${key}:test} [FOR TESTING ONLY]
				`

				if (rawArgs === 'test') return 'nsfw';
				this.nsfw = true;
				return '';

			case '\n':
				if (help) return `
					\`{${key}}\`

					Make a newline
					Example: \`{${key}}\`
				`

				return `\n`;

			case 'ping': {
				if (help) return `
					\`{${key}}\`

					Return bot latency to Discord API.
					Example: \`{${key}}\`
				`

				switch (rawArgs) {
					default:
						return Math.round(this.client.ws.ping);
				}
			}

			case 'haste':
			case 'hastebin': {
				if (help) return `
					\`{${key}}\`

					Create or get text/code from hastebin [Which hosted in wrmsr.io] [Maximum call was 5]
					Example: \`{${key}:thing}\`
				`

				this.hasteCalls++;
				if (this.hasteCalls > 5) return '[TOO MANY HASTE CALLS]';

				const keyMatch = rawArgs.match(/^(https?:\/\/wrmsr\.io\/)?([a-z0-9]{8,12})$/i);
				if (keyMatch) return await Parser.retrieveHaste(keyMatch[keyMatch.length - 1]);
				else return await Parser.createHaste(rawArgs);
			}

			default:
				if (rexLangs.includes(key)) {
					if (help) return `
						\`{lang}\`

						Evaluate code on rextester.com
						Example: \`{python:import os}\`

						Supported Programing languages:
						\`\`\`
						${rexLangs.join(', ')}
						\`\`\`
					`;

					if (!rawArgs) return;

					this.rexCalls++;
					if (this.rexCalls > 2) return '[TOO MANY REX CALLS]';

					const rexResult = await this.apiConnector('rextester', {
						args: {
							language: key,
							text: Parser.unescapeTag(rawArgs)
						}
					});

					return Parser.escapeTag(rexResult.toString());
				}

				if (help) {
					return 'Unknown key. Please do {help} for list available key';
				}

				return `{${key}${rawArgs ? `:${rawArgs}` : ''}}`;
		}
	}

	static timespanToMillis(timespan) {
		if (!timespan) return 0;

		let offset = 0;

		const months = Parser.extractNumber(timespan.match(/(\d+)mo/g));
		const millis = Parser.extractNumber(timespan.match(/(\d+)ms/g));

		const years = Parser.extractNumber(timespan.match(/(\d+)y/g));
		const weeks = Parser.extractNumber(timespan.match(/(\d+)w/g));
		const days = Parser.extractNumber(timespan.match(/(\d+)d/g));
		const hours = Parser.extractNumber(timespan.match(/(\d+)h/g));
		const minutes = Parser.extractNumber(timespan.match(/(\d+)m[^s]?/g));
		const seconds = Parser.extractNumber(timespan.match(/(\d+)s/g));

		offset += millis;
		offset += seconds * 1000;
		offset += minutes * 60 * 1000;
		offset += hours * 60 * 60 * 1000;
		offset += days * 24 * 60 * 60 * 1000;
		offset += weeks * 7 * 24 * 60 * 60 * 1000;
		offset += months * 30 * 24 * 60 * 60 * 1000;
		offset += years * 365 * 24 * 60 * 60 * 1000;

		if (timespan[0] === '-') offset *= -1;

		return offset;
	}

	static extractNumber(matches) {
		return (matches || [])
			.map(val => {
				return (val
					.match(/\d+/g) || [])[0];
			})
			.reduce((prev, curr) => prev + parseInt(curr), 0);
	}

	static compareLogic(value, compareValue, operator) {
		switch (operator) {
			case '=':
			case '==':
				return value === compareValue; // equal

			case '!':
			case '!=':
				return value !== compareValue; // not equal

			case '~':
				return (value || '').toLowerCase() === (compareValue || '').toLowerCase(); // equal ignore case

			case '>':
				return value > compareValue; // gt

			case '<':
				return value < compareValue; // lt

			case '>=':
				return value >= compareValue; // gt or equal

			case '<=':
				return value <= compareValue; // lt or equal

			case '?': {
				const regex = new RegExp(compareValue);
				return regex.test(value); // regex match
			}

			default:
				throw new Error(`Invalid if operator: \`${operator}\``);
		}
	}

	static escapeTag(tag) {
		return tag.replace(/{/g, '{\0').replace(/}/g, '\0}').replace(/\|/g, '\0|');
	}

	static unescapeTag(tag) {
		return tag.replace(/\0/g, '');
	}

	static async createHaste(content) {
		const { key } = await fetch('https://wrmsr.io/documents', { method: 'POST', body: content }).then(res => res.json());
		return `https://wrmsr.io/${key}`;
	}

	static async retrieveHaste(key) {
		const { data } = await fetch(`https://wrmsr.io/documents/${key}`).then(res => res.json());
		return data === undefined ? '[HASTE NOT FOUND]' : data;
	}
}

module.exports = Parser;