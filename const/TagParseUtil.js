const { isRegex } = require('util');

class TagPaserUtil {
    constructor(main) {
        this.main = main;
    }
    
    async parse(ctx, content, args) {
        
    }
    
    
    getParseObject(content) {
        const results = [];
        
        content.replace(/{user}|{user:(.+)}|{runis:(.*)}|{range:(\d+)\|(\d+)}|{server}|{date:(\d+)\|(\d+)\|(\d+)}|{channel}|{randuser}|{attach:(.*)}/gm)
    }
    
    async getRunis(content) {
        let result;
        
        const [match, script] = /{runis:(.*)}/gm.exec(content);
        
        try {
            result = await this.main.fAPI('parse_tag', {
                args: {
                    text: script
                }
            })
        } catch (e) {
            throw e.message;
        }
    }
    
    getAttach(content) {
        const results = [];
        
        const [match, url] = /{attach:(.*)}/gm.exec(content);
        
        if (!this.main.utils.isURL(url)) throw `${url} is not valid URL`;
        
        results.push(url);
        
        content = content.replace(/{attach:(.*)}/gm, '')
        
        return { content, results }
    }
}